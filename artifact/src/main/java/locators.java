import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class locators
extends singletonJava{

    public static void main(String[] args) {
        singletonJava.setupClass();
        singletonJava.getInstance();

        System.setProperty("webdriver.gecko.driver", "/Users/user/Downloads/geckodriver/");
        WebDriver driver = new FirefoxDriver();

        driver.get("https://www.bbc.com/");

        //search input css
        WebElement I = driver.findElement(By.cssSelector("input#orb-search-q"));
        if (I.isDisplayed()) {
            System.out.println("the 1st element found with the help of css");
        }

        //search input xpath
        WebElement Ixpath = driver.findElement(By.id("orb-search-q"));
        if (Ixpath.isDisplayed()) {
            System.out.println("the 1st element found with the help of xpath");
        }

        //weather section css
        WebElement I2 = driver.findElement(By.cssSelector("div#orb-nav-links.orb-nav-section.orb-nav-links.orb-nav-focus>ul>li.orb-nav-weather"));
        if (I2.isDisplayed()) {
            System.out.println("the 2nd element found with the help of css");
        }

        //weather section xpath
        WebElement I2xpath = driver.findElement(By.xpath("//div[@class='orb-nav-section orb-nav-links orb-nav-focus']//ul/li[@class='orb-nav-weather']"));
        if (I2xpath.isDisplayed()) {
            System.out.println("the 2nd element found with the help of xpath");
        }


        //picture section xpath
        WebElement I3 = driver.findElement(By.xpath("//div[@class='media media--hero media--primary media--overlay block-link']//a[@class='block-link__overlay-link']"));
        if (I3.isDisplayed()) {
            System.out.println("the 3rd element found with the help of xpath");
        }

        //picture section css
        WebElement I3css = driver.findElement(By.cssSelector("section>div>ul>li.media-list__item.media-list__item--1"));
        if (I3.isDisplayed()) {
            System.out.println("the 3rd element found with the help of css");
        }


        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        //date by xpath
        WebElement I4 = driver.findElement(By.xpath("//section[@class='module module--header']/h2/text()"));
        if (I4.isEnabled()) {
            System.out.println("the 4t element found with xpath");
        }


        //date by xpath NOT WORK!!!
        WebElement I4css = driver.findElement(By.cssSelector(".module.module--header h2 span"));
        System.out.println(I4css.getText());
        if (I4css.isEnabled()) {
            System.out.println("the 4t element found with css");
        }


        //list by xpath
        List<WebElement> I5 = driver.findElements(By.xpath("//div[@class='orb-nav-section orb-nav-links orb-nav-focus']//li[position() mod 2 = 0 and @class[not(contains(.,'orb-nav-hide'))]]"));

        for (WebElement e : I5) {
            if (e.isDisplayed())
                System.out.println(e.getTagName() + "    " + e.getText());
        }

        //list by css
        List<WebElement> I5css = driver.findElements(By.cssSelector(".orb-nav-section.orb-nav-links.orb-nav-focus li:nth-child(even):not([class*='orb-nav-hide'])"));

        for (WebElement e : I5) {
            if (e.isDisplayed())
                System.out.println(e.getTagName() + "    " + e.getText());
        }

        driver.close();
    }
}
