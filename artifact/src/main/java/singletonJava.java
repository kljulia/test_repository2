import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class singletonJava {
    public static WebDriver driver = null;


    public static void setupClass() {

        WebDriverManager.chromedriver().setup();
    }

    public static WebDriver getInstance() {
        if (driver == null) {
  //          System.setProperty("webdriver.chrome.driver", "/Users/user/Downloads/chromedriver/");

            driver = new ChromeDriver();
        }
        return driver;
    }
}