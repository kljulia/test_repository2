import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class displayedOrEnabled extends singletonJava {

    public static void main(String[] args)
    {
        System.out.println("Enter '1' to use method with 'isEnabled', '2' for isDisplayed");
        Scanner in = new Scanner(System.in);
        int blabla =in.nextInt();


        singletonJava.setupClass();
        singletonJava.getInstance();

        driver.get("https://www.bbc.com/");
        WebElement button = driver.findElement(By.cssSelector("input#orb-search-q"));


        if (blabla==1)
        {
            Wait wait = new FluentWait(driver)
                .withTimeout(45, TimeUnit.SECONDS)
                .pollingEvery(5, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
            isEnabl(button);
        }
        else {
            Wait wait = new FluentWait(driver)
                .withTimeout(45, TimeUnit.SECONDS)
                .pollingEvery(5, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        isDispl(button);
        }
        driver.close();
    }

    public static void isEnabl( WebElement b) {

        if (b.isEnabled())
        {
            System.out.println("element is enabled");
        } else {
            System.out.println("element is not enabled");
        }
    }

    public static void isDispl(WebElement b)
    {
        if (b.isDisplayed())
        {
            System.out.println("element is displayed");
        } else {
            System.out.println("element is not displayed");
        }
    }
}

