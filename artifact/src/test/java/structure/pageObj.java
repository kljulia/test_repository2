package structure;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class pageObj {
    WebDriver driver;


    public static By locationLink =By.cssSelector(".geolink__reg");
    public static By locationInput=By.cssSelector(".input__control.input__input");
    // public static By dropdownList=By.cssSelector(".popup.popup_animate_no.popup_theme_ffffff.popup_autoclosable_yes.popup_adaptive_yes.input__popup.input__popup_type_geo.input__popup_fade_yes.i-bem.popup_js_inited.popup_to_bottom");
    public static By dropdownList=By.cssSelector(".b-autocomplete-item.b-autocomplete-item_type_geo.b-autocomplete-item_subtype_region.i-bem.b-autocomplete-item_js_inited");
    public static By elseLink = By.cssSelector(".home-link.home-link_blue_yes.home-tabs__link.home-tabs__more-switcher.dropdown2__switcher");
    public static By elseList = By.cssSelector(" .home-tabs__more-item");

    public pageObj(WebDriver driver){
        this.driver=driver;
    }
}
