package structure;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class structure_Test extends singleton {
    List<WebElement> arrayParis;

    @Test
    public void test11() throws InterruptedException {
        List<String> listNames1 = null, listNames2 = null;

        singleton.getInstance();

        driver.get("https://yandex.by/");

        driver.findElement(pageObj.locationLink).click();

        WebElement elem1 = driver.findElement(pageObj.locationInput);
        elem1.clear();
        elem1.sendKeys("Лондон");

        WebDriverWait webDriverWait =  new WebDriverWait(driver, 10);
        webDriverWait
                .until(ExpectedConditions.visibilityOfElementLocated(pageObj.dropdownList));

        List<WebElement> array = driver.findElements(pageObj.dropdownList);
       array.get(0).click();


        webDriverWait
                .until(ExpectedConditions.visibilityOfElementLocated(pageObj.elseLink));

        driver.findElement(pageObj.elseLink).click();

        List<WebElement> list1 = driver.findElements(pageObj.elseList);

        for (int i = 0; i < list1.size(); i++) {
            listNames1 = Arrays.asList(list1.get(i).getText());
        }

        driver.findElement(pageObj.locationLink).click();

        webDriverWait
                .until(ExpectedConditions.visibilityOfElementLocated(pageObj.locationInput));

        WebElement elem2 = driver.findElement(pageObj.locationInput);
        elem2.clear();
        webDriverWait
                .until(ExpectedConditions.textToBe(pageObj.locationInput,""));


        elem2.sendKeys("Париж");


        webDriverWait
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(pageObj.dropdownList,1));


        List<WebElement>  arrayParis = driver.findElements(pageObj.dropdownList);
        arrayParis.get(0).click();


        webDriverWait
                .until(ExpectedConditions.visibilityOfElementLocated(pageObj.elseLink));
        driver.findElement(pageObj.elseLink).click();

        List<WebElement> list2 = driver.findElements(pageObj.elseList);

        for (int i = 0; i < list2.size(); i++) {
            listNames2 = Arrays.asList(list2.get(i).getText());
        }
        Assert.assertEquals(listNames1, listNames2, "lists are not the same");
    }

    @AfterSuite
    public void Quit() {
        driver.quit();
    }
}
