package pullTest1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import structure.pageObj;
import structure.singleton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class structure_Test extends singleton1 {

    String validLoginname="AutotestUser";
    String notValidLoginName="NotAutotestUser";

    String validPass="AutotestUser123";
    String notValidPass="NotAutotestUser123";

    public static final String BASE_URL = "https://yandex.by/";


    public void clickOnLoginButton(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.loginBtn));
        clickOnElement(driver.findElement(pageObj1.loginBtn));
    }

    public void clickOnLoginButton2(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.loginBtn2));
        clickOnElement(driver.findElement(pageObj1.loginBtn2)); }

    public void clickOnloginToAccountBtn(){
        clickOnElement(driver.findElement(pageObj1.loginToAccountBtn));
    }
    public void clickOnElement(WebElement webElement){
        webElement.click();
    }

    public void enteringValidLogin(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.loginInput));
        driver.findElement(pageObj1.loginInput).sendKeys(validLoginname);
    }
    public void enteringNotValidLogin(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.loginInput));
        driver.findElement(pageObj1.loginInput).sendKeys(notValidLoginName); }

    public void enteringValidPass(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.passInput));
        driver.findElement(pageObj1.passInput).sendKeys(validPass);
    }
    public void enteringNotValidPass(){

        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.passInput));
        driver.findElement(pageObj1.passInput).sendKeys(notValidPass);
    }

    public void clickingOnUsername(){

        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.userName));
        driver.findElement(pageObj1.userName).click();
    }

    public void clickingOnSignOut(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(pageObj1.userNamedropdowm));
        List<WebElement> array=  driver.findElements(pageObj1.userNamedropdowm);
        array.get(3).click();    }


    public void waitingForTitleDownloading(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.titleContains("Яндекс"));
    }
    public void waitingForErrorMessage(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.errorMsg));
    }

    public void waitingForUserNameVisible(){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.userName));
    }
    @Test(priority=2)
    public void login()  {

        clickOnLoginButton();

        enteringValidLogin();

        clickOnLoginButton2();

        enteringValidPass();

        clickOnloginToAccountBtn();

        waitingForUserNameVisible();

        Assert.assertEquals(driver.findElement(pageObj1.userName).getText(),validLoginname);
    }

    @Test(priority=1)
    public void logout() {


        driver.switchTo().activeElement();
        driver.findElement(By.cssSelector("* /deep/ #clearBrowsingDataConfirm")).click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOnLoginButton();

        enteringValidLogin();

        clickOnLoginButton2();

        enteringValidPass();

        clickOnloginToAccountBtn();

        clickingOnUsername();

        clickingOnSignOut();

        waitingForTitleDownloading();

        Assert.assertEquals(driver.getCurrentUrl(),BASE_URL);
    }

    @Test
    public void notValidPass() {

        clickOnLoginButton();

        enteringValidLogin();

        clickOnLoginButton2();

        enteringNotValidPass();

        clickOnloginToAccountBtn();

        waitingForErrorMessage();

        Assert.assertEquals(driver.findElement(pageObj1.errorMsg).getText(),"Неверный пароль");
    }

    @Test
    public void notExistingUser() {

        clickOnLoginButton();

        enteringNotValidLogin();

        clickOnLoginButton2();

        waitingForErrorMessage();

        Assert.assertEquals(driver.findElement(pageObj1.errorMsg).getText(),"Такого аккаунта нет");
    }

    @Test
    public void navigation()  {

       List<WebElement> navigationTab= driver.findElements(pageObj1.navTab);

        navigationTab.get(0).click();

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));

        Assert.assertEquals(driver.getCurrentUrl(),"https://yandex.by/video/");

        driver.switchTo().window(tabs2.get(0));

        //picture
        driver.findElements(pageObj1.navTab).get(1).click();
        Assert.assertEquals(driver.getCurrentUrl(),"https://yandex.by/images/");
        driver.navigate().back();

        //news
        driver.findElements(pageObj1.navTab).get(2).click();
        Assert.assertEquals(driver.getCurrentUrl(),"https://news.yandex.by/");
        Assert.assertTrue(driver.getCurrentUrl().contains("https://news.yandex."));

        driver.navigate().back();

        //maps
        driver.findElements(pageObj1.navTab).get(3).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("https://yandex.by/maps/"));
        driver.navigate().back();

        //market
        driver.findElements(pageObj1.navTab).get(4).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("https://market.yandex.by/"));
        driver.navigate().back();

        //translate
        driver.findElements(pageObj1.navTab).get(5).click();
        Assert.assertEquals(driver.getCurrentUrl(),"https://translate.yandex.by/");
        driver.navigate().back();

        //music
        driver.findElements(pageObj1.navTab).get(6).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("https://music.yandex.by"));
    }

    @Test
    public void changingToEnglishLanguage()  {

        driver.findElement(pageObj1.languageLabel).click();

        driver.findElement(pageObj1.elseLabel).click();

        driver.findElement(pageObj1.dropdownBtn).click();

        driver.findElement(pageObj1.englishOption).click();

        driver.findElement(pageObj1.saveLanguageBtn).click();

        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.languageLabel));

        driver.findElement(pageObj1.languageLabel).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(pageObj1.elseLabel));

        driver.findElement(pageObj1.elseLabel).click();

        Assert.assertEquals(driver.findElement(pageObj1.dropdownBtn).getText(),"English");
    }

    @BeforeMethod
    public void beforeTest(){
        System.out.println("Before Test Working....");

        driver.get(BASE_URL);


        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
