package pullTest1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.sql.SQLOutput;

public class singleton1 {
    public static WebDriver driver = null;


    @BeforeClass
    public static WebDriver getInstance() {
        if (driver == null) {
            driver = new ChromeDriver();
        }
        return driver;
    }


 //   @BeforeSuite
    public static void setupClass() {

        WebDriverManager.chromedriver().setup();
    }



    @BeforeSuite
    public void beforeSuite(){
        singleton1.setupClass();
        singleton1.getInstance();
    }

//    @BeforeTest
//    public void beforeTest(){
//        System.out.println("Before Test Working....");
//        driver.get("https://yandex.by/");
//    }



    @AfterSuite
    public void Quit() {
        driver.quit();
    }
}