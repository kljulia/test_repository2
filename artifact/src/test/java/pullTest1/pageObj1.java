package pullTest1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class pageObj1 {
    WebDriver driver;

    public static By loginBtn =By.cssSelector("" + ".button.desk-notif-card__login-enter-expanded.button_theme_gray.i-bem");
    public static By loginInput=By.id("passp-field-login");
    public static By loginBtn2=By.cssSelector(".passp-button.passp-sign-in-button");
    public static By passInput=By.id("passp-field-passwd");
    public static By loginToAccountBtn=By.cssSelector(".control.button2.button2_view_classic.button2_size_l.button2_theme_action.button2_width_max.button2_type_submit.passp-form-button");


    public static By userName=By.cssSelector(".mail-User-Name");
    public static By errorMsg=By.cssSelector(".passp-form-field__error");


    public static By userNamedropdowm=By.cssSelector(".b-mail-dropdown__item");
    public static By navTab=By.cssSelector(".home-link.home-link_blue_yes.home-tabs__link.home-tabs__search");


    public static By languageLabel=By.cssSelector(".link.i-bem.link_black_yes.link_pseudo_yes");
    public static By elseLabel=  By.xpath("//span[@class='menu__text'][contains(text(),'ещ')]");
    public static By dropdownBtn=By.cssSelector(".button.select__button.button_theme_normal.button_arrow_down.button_size_m.i-bem");

    public static By englishOption=   By.xpath("//span[@class='select__text'][contains(text(),'English')]");
    public static By saveLanguageBtn=    By.cssSelector(".button.form__save.button_theme_action.button_size_m.i-bem");


    public pageObj1(WebDriver driver){
        this.driver=driver;
    }
}
