package assertions;

import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import org.testng.collections.Lists;

import java.util.List;

public class CustomAssertion extends Assertion {

    private List<String> assert_messages = Lists.newArrayList();

    public void onBeforeAssert(IAssert a) {
        assert_messages.add("BeforeAssert:" + a.getMessage());
    }

    public void onAfterAssert(IAssert a) {
        assert_messages.add("AfterAssert:" + a.getMessage());
    }

    public void onAssertSuccess(IAssert assertCommand) {
        assert_messages.add("OnlyOnAssertSuccess:" + assertCommand.getMessage());
    }

    public void onAssertFailure(IAssert assertCommand, AssertionError ex) {
        assert_messages.add("OnlyOnAssertFailure:" + assertCommand.getMessage());
    }

    public List<String> getAssertMessages() {
        return assert_messages;
    }
}