package assertions;

import org.testng.Assert;
import org.testng.annotations.Test;

public class assertions_Test {
    private CustomAssertion m_custom = new CustomAssertion();

    @Test
    public void testNumbers() {
        int number1 = 3, number2 = 30;
        m_custom.assertEquals(number1, number2, "numbers are not the same");
        //  m_custom.assertTrue(false, "some text");
    }

    @Test
    public void testStringsWholeOverlap() {
        String string1 = "cat";
        String string2 = "cat";

        Assert.assertEquals(string1, string2, "strings are not the same");
    }


    public boolean Compare() {
        String string1 = "cat";
        String string2 = "cat11";

        if (string2.contains(string1)) {
            return true;
        } else {
            return false;
        }
    }

    @Test
    public void testStringArrays() {

        String[] cats = {"Васька", "Кузя", "Барсик"};
        String[] cats2 = {"Васька", "Кузя", "Барсик"};

        Assert.assertEquals(cats, cats2, "arrays are not the same");
    }
}
